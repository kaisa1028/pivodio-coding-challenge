import useMediaRecorder from '@wmik/use-media-recorder'
import { useEffect, useRef, useState } from 'react'

const VideoRecorder = () => {
  let previewRef = useRef<HTMLVideoElement>(null)
  let playbackRef = useRef<HTMLVideoElement>(null)
  let [showPreview, setShowPreview] = useState(true)
  let [showPlayback, setShowPlayback] = useState(false)
  let [videoSrc, setVideoSrc] = useState<MediaDeviceInfo | null>(null)
  let [audioSrc, setAudioSrc] = useState<MediaDeviceInfo | null>(null)
  let [mediaStream, setMediaStream] = useState<MediaStream | undefined>(
    undefined
  )
  let [mediaDevices, setMediaDevices] = useState<MediaDeviceInfo[]>([])
  let [isModalOpen, setIsModalOpen] = useState(false)
  let { status, mediaBlob, stopRecording, startRecording, liveStream } =
    useMediaRecorder({
      recordScreen: false,
      blobOptions: { type: 'video/webm' },
      customMediaStream: mediaStream,
      mediaStreamConstraints: { video: true, audio: true },
      onStart: () => {
        console.log('onStart')
        setShowPreview(true)
        setShowPlayback(false)
      },
      onStop: () => {
        console.log('onStop')
        setShowPreview(false)
        setShowPlayback(true)
      },
    })

  useEffect(() => {
    if (previewRef.current && liveStream) {
      console.log('set live stream')
      previewRef.current.srcObject = liveStream
    }
    if (playbackRef.current && mediaBlob) {
      console.log('set media blob')
      playbackRef.current.src = URL.createObjectURL(mediaBlob)
    }
    navigator.mediaDevices.enumerateDevices().then((devices) => {
      if (mediaDevices.length === 0) {
        console.log('set media devices')
        setMediaDevices(devices)
      }
    })
    console.log(videoSrc)
    console.log(audioSrc)
    if (videoSrc || audioSrc) {
      if (!mediaStream) {
        navigator.mediaDevices
          .getUserMedia({
            video: videoSrc ? { deviceId: videoSrc } : false,
            audio: audioSrc ? { deviceId: audioSrc } : false,
          })
          .then((stream) => {
            console.log('set media stream')
            setMediaStream(stream)
          })
          .catch((err) => {
            console.log(err)
          })
      }
    }
  }, [
    audioSrc,
    liveStream,
    mediaBlob,
    mediaDevices.length,
    mediaStream,
    videoSrc,
  ])
  return (
    <div>
      <h1 className='text-center'>Video Recorder: {status}</h1>
      <div className='flex justify-center flex-col'>
        {showPreview && (
          <div className='flex justify-center'>
            <video ref={previewRef} autoPlay muted />
          </div>
        )}
        {showPlayback && (
          <div className='flex justify-center'>
            <video ref={playbackRef} autoPlay loop />
          </div>
        )}
        <button onClick={() => setIsModalOpen(true)}>
          Select Media Devices
        </button>
        <button
          type='button'
          onClick={() => {
            if (status !== 'recording') {
              if (!videoSrc || !audioSrc) {
                for (let i = 0; i < mediaDevices.length; i++) {
                  if (mediaDevices[i].kind === 'videoinput') {
                    setVideoSrc(mediaDevices[i])
                  }
                  if (mediaDevices[i].kind === 'audioinput') {
                    setAudioSrc(mediaDevices[i])
                  }
                }
              }
              startRecording()
            } else {
              stopRecording()

              mediaStream?.getTracks().forEach((track) => track.stop())
              //   setMediaStream(undefined)
            }
          }}
        >
          {status === 'recording' ? 'Stop Recording' : 'Start Recording'}
        </button>
        {mediaBlob && (
          <a
            className='text-center'
            href={URL.createObjectURL(mediaBlob)}
            download='video.webm'
            type='video/webm'
          >
            Download
          </a>
        )}
      </div>

      {isModalOpen && (
        <div
          className='fixed z-10 inset-0 overflow-y-auto'
          aria-labelledby='modal-title'
          role='dialog'
          aria-modal='true'
        >
          <div className='flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0'>
            <div
              className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity'
              aria-hidden='true'
            ></div>
            <span
              className='hidden sm:inline-block sm:align-middle sm:h-screen'
              aria-hidden='true'
            >
              &#8203;
            </span>
            <div className='inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full'>
              <div className='bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4'>
                <div className='sm:flex sm:items-start'>
                  <div className='mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left'>
                    <h3
                      className='text-lg leading-6 font-medium text-gray-900'
                      id='modal-title'
                    >
                      Select Media Devices
                    </h3>
                    <div className='mt-2'>
                      {/* Video selection */}
                      <div>
                        <label
                          className='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2'
                          htmlFor='video-device'
                        >
                          Select Video Device
                        </label>
                        <div className='relative'>
                          <select
                            className='block appearance-none w-full bg-white border border-gray-300 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
                            id='video-device'
                            onChange={(e) => {
                              console.log('set video src')
                              setVideoSrc(
                                mediaDevices.find(
                                  (device) => device.deviceId === e.target.value
                                ) || null
                              )
                            }}
                          >
                            {mediaDevices
                              .filter((device) => device.kind === 'videoinput')
                              .map((device, index) => (
                                <option key={index} value={device.deviceId}>
                                  {device.label}
                                </option>
                              ))}
                          </select>
                        </div>
                      </div>
                      {/* Audio selection */}
                      <div>
                        <label
                          className='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2'
                          htmlFor='audio-device'
                        >
                          Select Audio Device
                        </label>
                        <div className='relative'>
                          <select
                            className='block appearance-none w-full bg-white border border-gray-300 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
                            id='audio-device'
                            onChange={(e) => {
                              console.log('set audio src')
                              setAudioSrc(
                                mediaDevices.find(
                                  (device) => device.deviceId === e.target.value
                                ) || null
                              )
                            }}
                          >
                            {mediaDevices
                              .filter((device) => device.kind === 'audioinput')
                              .map((device, index) => (
                                <option key={index} value={device.deviceId}>
                                  {device.label}
                                </option>
                              ))}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse'>
                <button
                  type='button'
                  className='w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm'
                  onClick={() => setIsModalOpen(false)}
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default VideoRecorder
