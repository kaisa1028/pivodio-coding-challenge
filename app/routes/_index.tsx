import type { V2_MetaFunction } from '@remix-run/node'
import VideoRecorder from '~/components/VideoRecorder'

export const meta: V2_MetaFunction = () => {
  return [{ title: 'Video Recorder - PIVODIO coding challenge' }]
}

export default function Index() {
  return (
    <div className='flex items-center justify-center h-screen bg-gray-800'>
      <div className='bg-gray-200 p-6 rounded shadow-lg w-96'>
        <VideoRecorder />
      </div>
    </div>
  )
}
